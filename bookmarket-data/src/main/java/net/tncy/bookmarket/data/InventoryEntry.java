package net.tncy.bookmarket.data;

public class InventoryEntry {

    private Book book;
    private int quantity;
    private float averagePrice;
    private float currentPrice;

    public Book getBook() {
        return book;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getAveragePrice() {
        return averagePrice;
    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }
}
