package net.tncy.bookmarket.services;

import net.tncy.bookmarket.data.Book;

import java.util.ArrayList;
import java.util.List;

public class BookstoreService {
    public void createBookstore(){}
    public void deleteBookstore(){}
    public void getBookstoreById(){}
    public void findBookstoreByName(){}
    public void renameBookstore(){}
    public void addBookToBookstore(){}
    public void removeBookFromBookstore(){}
    public List<Book> getBookstoreInventory(){
        return new ArrayList<>();
    }
    public void getBookstoreCatalog(){}
}
